﻿using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;

namespace MainServer.Framework
{
    public class DBConnector
    {
        public SqlConnection GetConnection()
        {
            SqlConnection myConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["AppHarborSql"].ConnectionString);
            Trace.TraceError("SQL CONNECTION CREATED: " + myConnection.ConnectionString);
            return myConnection;
        }
        public bool isServerOnline()
        {
            using (SqlConnection con = new DBConnector().GetConnection())
            {
                con.Open();
                using (SqlCommand command = new SqlCommand("SELECT online FROM configs", con))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            //Console.WriteLine("{0} {1} {2}",
                            //reader.GetInt32(0));
                            int value = reader.GetInt32(0);
                            if (value == 1)
                            {
                                con.Close();
                                return true;
                            }
                        }
                    }
                }

                con.Close();
            }
            return false;
        }
    }
}