﻿using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Web.Http;

namespace MainServer
{
    public static class WebApiConfig
    {

        /*
          git add .
          git commit -m 'routing bug'
          git push appharbor master
        */

        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            GlobalConfiguration.Configuration.Formatters.XmlFormatter.MediaTypeMappings.Add(new QueryStringMapping("xml", "true", "application/xml"));
            GlobalConfiguration.Configuration.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}/{debug}",
                defaults: new { id = RouteParameter.Optional, debug = false }
            );

            // new_status
            config.Routes.MapHttpRoute(
                name: "StatusApi",
                routeTemplate: "api/{controller}/{new_status}/{api_key}",
                defaults: new { new_status = RouteParameter.Optional, api_key = RouteParameter.Optional }
);
        }

    }
}
