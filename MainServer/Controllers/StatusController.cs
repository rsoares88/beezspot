﻿using MainServer.Framework;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Web.Http;

namespace MainServer.Controllers
{
    public class StatusController : ApiController
    {

        class StatusReply
        {
            public bool status { get; set; }
            public int wasUpdated = 0;
        }

        [HttpGet]
        public IHttpActionResult ReturnServerStatus()
        {
            DBConnector connector = new DBConnector();

            bool isServerOnline = connector.isServerOnline();
            StatusReply reply = new StatusReply();
            reply.status = isServerOnline;
            return Ok(reply);
        }

        [Route("api/status/{new_status:int}/{api_key:string}")]
        [HttpGet]
        public IHttpActionResult UpdateServerStatus(int new_status, string api_code)
        {
            DBConnector connector = new DBConnector();

            using (SqlConnection con = connector.GetConnection())
            {
                con.Open();
                using (SqlCommand command = new SqlCommand("UPDATE configs SET online = @statusCode WHERE api_key = @apiCode", con))
                {
                    int status_clamp = ExtendedMethods.Clamp(0, 1, new_status);
                    command.Parameters.AddWithValue("@statusCode", status_clamp);
                    command.Parameters.AddWithValue("@apiCode", api_code);
                    int affected_rows= command.ExecuteNonQuery();

                    bool isServerOnline = connector.isServerOnline();
                    StatusReply reply = new StatusReply();
                    reply.status = isServerOnline; reply.wasUpdated = affected_rows;
                    Trace.TraceInformation("finished updating");

                    con.Close();
                    return Ok(reply);

                }
                
            }


        }

    }
}
