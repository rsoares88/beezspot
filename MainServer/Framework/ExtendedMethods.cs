﻿using System;

namespace MainServer.Framework
{
    public static class ExtendedMethods
    {
        public static int Clamp(int min, int max, int val)
        {
            if (val.CompareTo(min) < 0) return min;
            else if (val.CompareTo(max) > 0) return max;
            else return val;
        }
    }
}