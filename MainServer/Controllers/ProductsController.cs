﻿using MainServer.Framework;
using MainServer.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Web.Http;

namespace MainServer.Controllers
{
    public class ProductsController : ApiController
    {
        Product[] products = new Product[]
        {
            new Product { Id = 1, Name = "Tomato Soup", Category = "Groceries", Price = 1 },
            new Product { Id = 2, Name = "Yo-yo", Category = "Toys", Price = 3.75M },
            new Product { Id = 3, Name = "Hammer", Category = "Hardware", Price = 16.99M }
        };

        private Product[] db_products { get; set; }

        private Product[] PopulateProductes()
        {
            Trace.TraceError("PopulateProductes - INIT SQL");
            int value = -1;
            using (SqlConnection con = new DBConnector().GetConnection())
            {
                con.Open();
                using (SqlCommand command = new SqlCommand("SELECT online FROM configs", con))
                {
                    Trace.TraceError("inside SqlCommand");
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        Trace.TraceError("inside SqlCommand: " + reader.ToString());
                        while (reader.Read())
                        {
                            Trace.TraceError("inside ExecuteReader");
                            //Console.WriteLine("{0} {1} {2}",
                            //reader.GetInt32(0));
                            Trace.TraceError("results: " + reader.GetInt32(0));
                            value = reader.GetInt32(0);
                        }
                    }
                }

                con.Close();
            }

                    //debug code
                    return new Product[]
                    {
                    new Product { Id = 1, Name = "DB_Tomato Soup", Category = "Groceries", Price = 1 },
                    new Product { Id = 2, Name = "DB_Yo-yo", Category = "Toys", Price = 3.75M },
                    new Product { Id = 3, Name = "DB_CONFIG", Category = "ONLINE", Price = value },
                    new Product { Id = 4, Name = "DB_Hammer", Category = "Hardware", Price = 16.99M }
                    };
                    /*
                     SqlParameter myParam = new SqlParameter("@Param1", SqlDbType.VarChar, 11);
myParam.Value = "Garden Hose";
        
SqlParameter myParam2 = new SqlParameter("@Param2", SqlDbType.Int, 4);
myParam2.Value = 42;

SqlParameter myParam3 = new SqlParameter("@Param3", SqlDbType.Text);
myParam.Value = "Note that I am not specifying size. " + 
  "If I did that it would trunicate the text.";
                    */

                }

        public IEnumerable<Product> GetAllProducts()
        {
            return products;
        }

        public IHttpActionResult GetProduct(int id, bool debug)
        {
            Product product = null;
            if (debug)
            { product = products.FirstOrDefault((p) => p.Id == id); }
            else
            {
                db_products = PopulateProductes();
                product = db_products.FirstOrDefault((p) => p.Id == id);
            }

            if (product == null)
            {
                return NotFound();
            }
            return Ok(product);

        }

    }
}